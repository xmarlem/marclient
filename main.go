package main

import (
  "fmt"
  "net/http"

  "gitlab.com/xmarlem/marclient/gohttp"
)

var (
  githubHttpClient = getGithubClient()
)

func main() {
  getURL()
}

func getGithubClient() gohttp.Client {
  c := gohttp.New()
  commonHeaders := make(http.Header)
  commonHeaders.Set("Authorization", "Bearer ABC-123")

  c.SetHeaders(commonHeaders)
  return c
}

func getURL() {
  headers := make(http.Header)

  //headers.Set("Authorization", "Bearer ABC-123")

  response, err := githubHttpClient.Get("https://api.github.com", headers)

  if err != nil {
    panic(err)
  }
  fmt.Println(response.StatusCode)

}
